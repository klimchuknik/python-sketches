from random import randint

n = int(input("Enter the initial dimension of the matrix... "))
print()

dimension = 2*n-1
counter = randint(0, 9)

matrix = []
final_list = []

for i in range(dimension):
    matrix.append([])

    for j in range(dimension):
        matrix[i].append(counter)

        final_list.append(counter)
        counter = randint(0, 9)

# print(matrix)
# print()

# print(final_list)
# print()

print(' '.join(map(str, final_list)))
print()

for i in matrix:

    for j in i:
        print(j, end=' ')

    print()
